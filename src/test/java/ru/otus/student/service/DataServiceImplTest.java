package ru.otus.student.service;

import org.junit.jupiter.api.Test;
import ru.otus.student.model.FileWithQuestions;
import ru.otus.student.model.QuestionAndAnswer;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataServiceImplTest {


    @Test
    public void readDataFromFile() throws Exception {
        FileWithQuestions fileWithQuestions = new FileWithQuestions("./src/test/resources/questions.csv");
        DataService dataService = new DataServiceImpl(fileWithQuestions);
        List<QuestionAndAnswer> result = dataService.readDataFromFile();
        List<QuestionAndAnswer> expectedResult = new ArrayList<>();
        expectedResult.add(new QuestionAndAnswer("question one", "answer one"));
        expectedResult.add(new QuestionAndAnswer("question two", "answer two"));
        assertEquals(expectedResult,result);
    }

    @Test
    public void writeDataToFile() {
        FileWithQuestions fileWithQuestions = new FileWithQuestions("./src/test/resources/test.csv");
        DataService dataService = new DataServiceImpl(fileWithQuestions);
        dataService.writeDataToFile();
        Path path = Paths.get(fileWithQuestions.getFileName());
        File questions = new File(path.toString());
        Boolean isExistFile = questions.exists();
        assertEquals(true, isExistFile);
        questions.delete();
    }

}