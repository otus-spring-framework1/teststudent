package ru.otus.student;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.student.model.QuestionAndAnswer;
import ru.otus.student.service.DataService;
import ru.otus.student.service.DataServiceImpl;

import java.util.List;


public class Application {

    public static void main(String[] args) throws Exception {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        DataService dataService = context.getBean(DataServiceImpl.class);
        List<QuestionAndAnswer> questionAndAnswerList = dataService.readDataFromFile();
        printQuestions(questionAndAnswerList);
    }

    private static void printQuestions(List<QuestionAndAnswer> questionAndAnswerList) {
        questionAndAnswerList.forEach(element->{
            System.out.println(element.getQuestion());
        });
    }
}

