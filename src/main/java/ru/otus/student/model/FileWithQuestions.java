package ru.otus.student.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileWithQuestions {
    private String fileName;
}
