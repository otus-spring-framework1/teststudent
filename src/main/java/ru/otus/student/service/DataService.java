package ru.otus.student.service;

import ru.otus.student.model.QuestionAndAnswer;
import java.util.List;


public interface DataService {
    public void writeDataToFile();
    public List<QuestionAndAnswer>  readDataFromFile() throws Exception;
}
