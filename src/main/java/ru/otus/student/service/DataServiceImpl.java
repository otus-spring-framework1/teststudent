package ru.otus.student.service;

import com.opencsv.CSVReader;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import lombok.AllArgsConstructor;
import ru.otus.student.model.FileWithQuestions;
import ru.otus.student.model.QuestionAndAnswer;

import java.io.File;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
public class DataServiceImpl implements DataService {
    private FileWithQuestions fileWithQuestions;

    public List<QuestionAndAnswer> readDataFromFile() throws Exception {
        writeDataToFile();
        Path path = Paths.get(fileWithQuestions.getFileName());
        List<QuestionAndAnswer> questionAndAnswerList;

        try (
                Reader reader = Files.newBufferedReader(path);
                CSVReader csvReader = new CSVReader(reader);
        ) {
            questionAndAnswerList = getListQuestionAndAnswer(csvReader);
            System.out.println();
        }
        return questionAndAnswerList;
    }


    public void writeDataToFile() {
        Path path = Paths.get(fileWithQuestions.getFileName());
        File questions = new File(path.toString());
        if (!questions.exists()) {
            try (Writer writer = new FileWriter(path.toString())) {
                StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer)
                        .withSeparator(';')
                        .build();
                List<QuestionAndAnswer> list = addDefaultData();
                sbc.write(list);
            } catch (Exception ex) {
                System.out.println(ex.getStackTrace());
            }
        }
    }

    public List<QuestionAndAnswer> getListQuestionAndAnswer(CSVReader csvReader) throws Exception {
        List<QuestionAndAnswer> questionAndAnswerList = new ArrayList<>();
        String[] nextRecord;
        while ((nextRecord = csvReader.readNext()) != null) {
            String[] questionAndAnswerArray = nextRecord[0].split(";");
            String question = questionAndAnswerArray[0].replace('"',' ').trim();
            String answer = questionAndAnswerArray[1].replace('"',' ').trim();
            QuestionAndAnswer questionAndAnswer = new QuestionAndAnswer(question,answer);
            questionAndAnswerList.add(questionAndAnswer);
        }
        return questionAndAnswerList;
    }

    private List<QuestionAndAnswer> addDefaultData() {
        List<QuestionAndAnswer> questionAndAnswerList = new ArrayList<>();
        questionAndAnswerList.add(new QuestionAndAnswer("What year was founded Saint-Petersburg?",
                                                        "In 1703"));
        questionAndAnswerList.add(new QuestionAndAnswer("Who founded Saint-Petersburg?",
                                                        "Peter the Great"));
        questionAndAnswerList.add(new QuestionAndAnswer("What is the main street in Saint-Petersburg?",
                                                        "The Nevsky Prospect"));
        questionAndAnswerList.add(new QuestionAndAnswer("What is the biggest museum in Saint-Petersburg?",
                                                        "The Hermitage"));
        questionAndAnswerList.add(new QuestionAndAnswer("What is the highest building in the center of Saint-Petersburg?",
                                                        "The Saint Isaac's Cathedral"));
        return questionAndAnswerList;
    }
}

