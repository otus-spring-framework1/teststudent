FROM openjdk:11-jdk-slim
ADD target/TestStudent-1.0-SNAPSHOT-jar-with-dependencies.jar .
CMD ["java", "-jar", "TestStudent-1.0-SNAPSHOT-jar-with-dependencies.jar"]